/**
 * Created by Elaine Hong on 27/6/2016.
 */
// load Express

var express = require("express");

var port = 3000;
var staticDir =__dirname+"/public";
var i=2;
while(i<process.argv.length){
    switch (process.argv[i]){
        case"-v":
            console.info("Version :%d", version");
            break;
        case"-p":
            i=i+1;
            port= process.argv[i];
            break;
        case"-d":
            i=i+1;
            staticDir = process.argv[i];
            break;
        
        default:
    }
    i+=1;
}

//Create an instance of express application

var app = express();
/*Serve files from public directory
__dirname is the absolute path of the application directory
 */

app.use(express.static(__dirname + "/public"));



//Start the web server on port 3000

console.info("__dirname: %s", __dirname);

app.set("port", process.argv[2] || 3000);

app.listen(
    app.get("port"),
    function() {
        console.info("Application started on port %d",app.get("port"));
        console.info("")
    }
    
);